EESchema Schematic File Version 4
LIBS:SOIC8-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "SOIC-8"
Date ""
Rev "-"
Comp "Hot Mesh Solutions"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L myLib:8-SOIC U1
U 1 1 5BB6CC4E
P 6200 3300
F 0 "U1" H 6175 3425 50  0000 C CNN
F 1 "8-SOIC" H 6175 3334 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 6200 3300 50  0001 C CNN
F 3 "" H 6200 3300 50  0001 C CNN
	1    6200 3300
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Male J1
U 1 1 5BB6CFD4
P 5150 3600
F 0 "J1" H 5256 3878 50  0000 C CNN
F 1 "Conn_01x04_Male" H 5256 3787 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 5150 3600 50  0001 C CNN
F 3 "~" H 5150 3600 50  0001 C CNN
	1    5150 3600
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Male J2
U 1 1 5BB6D03E
P 6500 3700
F 0 "J2" H 6473 3580 50  0000 R CNN
F 1 "Conn_01x04_Male" H 6473 3671 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 6500 3700 50  0001 C CNN
F 3 "~" H 6500 3700 50  0001 C CNN
	1    6500 3700
	-1   0    0    1   
$EndComp
Wire Wire Line
	6300 3500 5950 3500
Wire Wire Line
	5350 3500 5700 3500
Wire Wire Line
	5350 3600 5700 3600
Wire Wire Line
	5350 3700 5700 3700
Wire Wire Line
	5350 3800 5700 3800
Wire Wire Line
	6300 3600 5950 3600
Wire Wire Line
	5950 3700 6300 3700
Wire Wire Line
	6300 3800 5950 3800
$EndSCHEMATC
